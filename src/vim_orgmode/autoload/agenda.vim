" Jump from agenda buffer to main org buffer and selected todo
function agenda#AgendaJump()
    let l:str=split(getline('.'))
    let l:file=str[0] . '.org'
    let l:num_part=str[-1]
    let l:num=split(num_part, 'L')[-1]
    if stridx(num_part, 'L') >= 0
        execute 'wincmd w | e ' . l:file ' | norm ' . l:num . 'Gzvw'
    else
        echo 'Wrong agenda line:' . l:file . l:num
    endif
endfunction
