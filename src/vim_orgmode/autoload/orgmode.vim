function orgmode#fold_text() abort
    return getline(v:foldstart) . '...'
endfunction

function orgmode#fold_expr(lnum)
    let level = strlen(matchstr(getline(a:lnum), '\v^\s*\zs\*+'))
    if level > 0
        return '>'.level
    else
        return '='
    endif
endfunction
