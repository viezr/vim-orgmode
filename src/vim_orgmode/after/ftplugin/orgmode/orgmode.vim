" setlocal foldmethod=syntax
setlocal foldmethod=expr
setlocal foldtext=orgmode#fold_text()
setlocal foldexpr=orgmode#fold_expr(v:lnum)
setlocal fillchars-=fold:-

silent! nunmap <Leader>da
nnoremap <buffer> <Leader>ag :call Agenda()<CR>
nnoremap <buffer> <Leader>ar :call Archive()<CR>
nnoremap <buffer> <Leader>ss :call AgendaSchedule()<CR>
nnoremap <buffer> <Leader>sd :call AgendaDeadline()<CR>
nnoremap <buffer> <Leader>sc :put =strftime('CLOSE: [%Y-%m-%d %a %H:%M]')<CR>
nnoremap <buffer> <Leader>td 0wciwDONE<Esc>:put =strftime('CLOSED: [%Y-%m-%d %H:%M]')<CR>
nnoremap <buffer> <Leader>tc 0wciwCANCEL<Esc>:put =strftime('CLOSED: [%Y-%m-%d %H:%M]')<CR>
nnoremap <buffer> <Leader>tt 0wciwTODO<Esc>
nnoremap <buffer> <Leader>ta 0wciwACTIVE<Esc>
nnoremap <buffer> <Leader>tw 0wciwWAIT<Esc>


" Agenda buffer
function Agenda()
    vnew
    execute 'vertical resize 80'
    setlocal buftype=nofile
    setlocal noswapfile
    " agenda command is an external script that returns agenda text
    %!agenda
    setlocal ft=agenda
    map <buffer> <Leader>ar :%!agenda<CR>
    nmap <buffer> <Leader>x :call agenda#AgendaJump()<CR>
endfunction

" Agenda buffer
function Archive()
    execute 'silent norm dd'
    let l:file=expand('%:p') . '_archive'
    let l:text=split(getreg('"'), '\n')
    let l:data=[''] + l:text
    call writefile(data, file, 'a')
    echo 'Moved ' . len(text) . ' lines to ' . file
endfunction

" Add deadline entry in next line
function AgendaDeadline()
    let l:date=strftime('<%Y-%m-%d>')
    execute 'norm j'
    let l:str=getline('.')
    if stridx(str, 'SCHEDULED') >= 0
        execute 'norm ^i' . 'DEADLINE: ' . date . ' '
    else
        execute 'norm ko' . 'DEADLINE: ' . date . ''
    endif
endfunction

" Add schedule entry in next line
function AgendaSchedule()
    let l:date=strftime('<%Y-%m-%d>')
    execute 'norm j'
    let l:str=getline('.')
    if stridx(str, 'DEADLINE') >= 0
        execute 'norm ^i' . 'SCHEDULED: ' . date . ' '
    else
        execute 'norm ko' . 'SCHEDULED: ' . date . ''
    endif
endfunction
