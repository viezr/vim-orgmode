if exists("b:current_syntax")
  finish
endif

syn match Concealed '.\{4\}$' conceal
setlocal conceallevel=3

syn match orgDeadline /^.\+\[D\]/
syn keyword orgTodoTodo TODO
syn keyword orgTodoDone DONE
syn keyword orgTodoActive WAIT ACTIVE
syn keyword orgTodoCancel CANCEL
syn match orgUrgentType /\[\#[ABC]\]\s/
syn match orgTags /\s:\w\+:\s/

syn match orgSched /[1-2][0-9]\{3}-[0-2][0-9]-[0-3][0-9]/
syn match orgWeekday /^\w.\+/
syn match orgToday ".*\zeLTOD"
syn match Concealed 'LTOD$' conceal

hi def link orgTodoTodo         GruvboxRedBold
hi def link orgTodoDone         GruvboxGreenBold
hi def link orgTodoActive       GruvboxPurpleBold
hi def link orgTodoCancel       GruvboxYellowBold
hi def link orgTags             Comment
hi def link orgUrgentType       Type
hi def link orgDeadline         GruvboxOrangeBold

hi def link orgSched            GruvboxBlue
hi def link orgWeekday          GruvboxBlueBold
hi def link orgToday            GruvboxGreenBold
