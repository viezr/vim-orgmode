" Vim syntax file
" Language: Org-mode
" Maintainer: viezr
" Latest revision: 24 Dec 2024

if exists("b:current_syntax")
  finish
endif


syn region orgHeadBlock start=/^\*\+\s/ end=/$/ transparent keepend
    \ contains=orgHeadStart,orgTodoTodo,orgTodoDone,orgTodoActive,orgTodoCancel,
    \ orgUrgentType,orgTags,orgTitle

syn match orgHeadStart /^\*\+\s/ contained
syn match orgCommand /^\$\s.\+/
syn match orgMdTitle /^\#\s.\+/
syn match orgMdSubTitle /^\##\+\s.\+/
syn keyword orgTodoTodo TODO contained
syn keyword orgTodoDone DONE contained
syn keyword orgTodoActive WAIT ACTIVE contained
syn keyword orgTodoCancel CANCEL contained
syn match orgUrgentType /\[\#[ABC]\]\s/ contained
syn match orgTags /\s:\w\+:$/ nextgroup=orgTitle contained
syn match orgTitle /\w\+\(\-\w\+\)\=/ contained

syn match orgSched /<[1-2][0-9]\{3}-[0-2][0-9]-[0-3][0-9]\(\s\w\{3}\)\=\(\s[0-2][0-9]:[0-5][0-9]\)\=\(\s+[1-9][ymwd]\)\=>/
syn match orgClosed /\[[1-2][0-9]\{3}-[0-2][0-9]-[0-3][0-9]\(\s\w\{3}\)\=\s[0-2][0-9]:[0-5][0-9]\]/

hi def link orgTitle            GruvboxBlue
hi def link orgHeadBlock        GruvboxBlue
hi def link orgHeadStart        GruvboxPurple
hi def link orgCommand          GruvboxGreen
hi def link orgTodoTodo         GruvboxRedBold
hi def link orgTodoDone         GruvboxGreenBold
hi def link orgTodoActive       GruvboxPurpleBold
hi def link orgTodoCancel       GruvboxYellowBold
hi def link orgTags             Comment
hi def link orgUrgentType       Type
hi def link orgMdTitle          GruvboxYellowBold
hi def link orgMdSubTitle       GruvboxGreenBold

hi def link orgSched            Underlined
hi def link orgClosed           Comment
